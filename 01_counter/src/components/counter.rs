use egui::Ui;

pub struct Counter {
    value: i64,
}

impl Counter {
    pub fn new() -> Self {
        Self {
            value: 0,
        }
    }

    pub fn show(&mut self, ui: &mut Ui) {
        ui.horizontal(|ui: &mut Ui| { // TODO
            if ui.button("-").clicked() {
                self.value -= 1;
            }

            ui.label(self.value.to_string());

            if ui.button("+").clicked() {
                self.value += 1;
            }
        });
    }
}
