mod app;
mod components;
use app::App;

#[cfg(not(target_arch = "wasm32"))]
#[async_std::main]
async fn main() {
    tracing_subscriber::fmt::init(); // Log to stdout (if you run with 'RUST_LOG=debug')

    // Options controlling the behavior of a native window.
    // Only a single native window is currently supported.
    let native_options = eframe::NativeOptions::default();

    // This is how you start a native (desktop) app.
    // The first argument is name of your app, used
    // for the title bar of the native window and
    // the save location of persistence (see App::save).
    eframe::run_native(
        "egui_demo", // App title
        native_options, // options of the behavior of the native window
        // The third argument is eframe::AppCreator, which equals to this type definition:
        // pub type AppCreator = Box<dyn FnOnce(&CreationContext<'_>) -> Box<dyn App>>;
        // Its a Box that takes in a closure that returns Box with App
        // The closure takes CreationContext as an argument, which is used for setting up and initialize
        // your  app, for example loading state from the storage
        Box::new(|_cc| Box::new(App::new())),
    ).unwrap();
}

#[cfg(target_arch = "wasm32")]
#[async_std::main]
async fn main() {
    console_error_panic_hook::set_once();  // Make sure panics are logged using 'console.error'.
    tracing_wasm::set_as_global_default(); // Redirect tracing to console.log and friends

    let web_options = eframe::WebOptions::default();

    // Spawns a Future<Output = ()> on the current thread.
    // This is the best way to run a Future in Rust without sending it to JavaScript
    wasm_bindgen_futures::spawn_local(async {
        // Install event listeners to register different input events
        // and start running the given app.
        // This is the entry-point for all the web-assembly.
        // This is called from the HTML.
        // It loads the app, installs some callbacks, then returns.
        // It returns a handle to the running app that can be stopped calling `AppRunner::stop_web`.
        // You can add more callbacks like this if you want to call in to your code.
        eframe::start_web(
            "app",
            web_options,
            Box::new(|_cc| Box::new(App::new())),
        )
        .await
        .expect("Failed to start egui_demo");
    });
}
