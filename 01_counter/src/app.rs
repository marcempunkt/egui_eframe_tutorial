use eframe::Frame;
use egui::{Context, Ui};
use crate::components::counter::Counter;

pub struct App {
    components: Components,
}

struct Components {
    counter: Counter,
}

impl App {
    pub fn new() -> Self {
        Self {
            components: Components {
                counter: Counter::new(),
            }
        }
    }
}

// Implement this trait to write apps that can be compiled for both web/wasm and desktop/native
impl eframe::App for App {
    // Called each time the UI needs repainting, which may be many times per second.
    // Context: Your handle to egui.
    // Frame: Represents the surroundings of your app.
    //        It provides methods to inspect the surroundings
    //        (are we on the web?), allocate textures,
    //        and change settings (e.g. window size).
    fn update(&mut self, ctx: &Context, _: &mut Frame) {
        egui::CentralPanel::default().show(ctx, |ui: &mut Ui| {
            ui.heading("Professional counter app");
            self.components.counter.show(ui);
        });
    }
}
